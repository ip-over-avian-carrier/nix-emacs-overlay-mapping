from osproc import execProcess, startProcess, ProcessOption, close, waitForExit
from os import getEnv, paramCount, paramStr
import std/jsonutils
import std/json
import std/strformat
import strutils
import std/enumerate
import std/sequtils
import std/sugar
from htmlgen as h import nil
from std/times import format

let
    OVERLAY_URL = "https://github.com/nix-community/emacs-overlay"
    SAVANNAH_URL = "https://git.savannah.gnu.org/cgit/emacs.git"
    OPTIONS: set[ProcessOption] = {poUsePath, poEchoCmd}

type Hash = tuple[hash: string, ts: string]
type HashInfo = object
    overlay_rev: string
    overlay_ts: string
    emacs_rev: string
    emacs_version: string

proc m(msg: string): void =
    echo "[*] " & msg

proc rungit(args: openArray[string] = []; extraOptions: set[ProcessOption] = {}, timeout: int = -1, noecho: bool = false): int {.raises: [Exception], tags: [ExecIOEffect, ReadIOEffect, RootEffect].} =
  var options = OPTIONS + extraOptions

  if noecho:
    options = options - {poEchoCmd}

  if poEchoCmd in options:
    write(stdout, "+ ")
  let process = startProcess("git", args=args, options=options)
  process.waitForExit

proc rungitOutput(args: openArray[string] = []; extraOptions: set[ProcessOption] = {}, timeout: int = -1, noecho: bool = false): string {.raises: [Exception], tags: [ExecIOEffect, ReadIOEffect, RootEffect].} =
  var options = OPTIONS + {poStdErrToStdOut} + extraOptions

  if noecho:
    options = options - {poEchoCmd}

  if poEchoCmd in options:
    write(stdout, "+ ")
  execProcess("git", args=args, options=options)


proc ensureLocalCopy(url: string, at: string): void =
    m("Ensuring local copy of repository")
    if not os.dirExists(at):
        m("Copy did not exist, cloning...")
        discard rungit(args=["clone", url, at])
        discard rungit(args=["-C", at, "config", "advice.detachedHead", "false"])
    else:
        m("Copy exists, updating...")
        var output = rungitOutput(args=["-C", at, "remote", "get-url", "origin"])
        output.stripLineEnd
        m(&"URL is {output}")
        assert output == url
        discard rungit(args=["-C", at, "remote", "get-url", "origin"])

proc ensureAtCommit(commit: string, at: string): void =
  m("Making sure that HEAD is reattached...")
  if rungit(args=["-C", at, "symbolic-ref", "-q", "HEAD"]) == 1:
    discard rungit(args=["-C", at, "checkout", "master"])

  if commit == "latest":
    m("Updating repo to latest commit")
    discard rungit(args=["-C", at, "pull"], extraOptions={poParentStreams})
  else:
    m(&"Updating repo to commit: {commit}")
    discard rungit(args=["-C", at, "checkout", commit])

proc loadHashes(at: string): seq[Hash] =
    m("Loading all hashes from repository...")
    var content = rungitOutput(args=["-C", at, "log", "--format=%H~%cI", "repos/emacs/emacs-master.json"])
    content.stripLineEnd
    for hashline in split(content, "\n"):
        let lp = split(hashline, '~')
        result.add((hash: lp[0], ts: lp[1]))

proc loadEmacsMasterInfo(at: string, hash: Hash): HashInfo =
    let data = rungitOutput(args=["-C", at, "show", &"{hash.hash}:repos/emacs/emacs-master.json"], noecho=true).strip.parseJson
    return HashInfo(
        overlay_rev: hash.hash,
        overlay_ts: hash.ts,
        emacs_rev: data["rev"].getStr(),
        emacs_version: data["version"].getStr()
    )

proc toIndex(to: string, data: seq[HashInfo]): void =
    m("Generated html file")
    # TODO Doctype
    var head = h.head(
        h.meta(charset = "utf-8"),
        h.meta(`http-equiv` = "x-ua-compatible", content = "ie=edge"),
        h.title("Emacs Overlay Mapping"),
        h.meta(name = "description", content = "Emacs Overlay Mapping"),
        h.meta(name = "viewport", content = "width=device-width, initial-scale=1"),
        h.link(rel = "stylesheet", href = "./pico.min.css"),
        h.style(".tbl-r { display: grid; grid-template-columns: max-content max-content; grid-column-gap: 8px; }"),
    )

    var body = h.body(
        h.`div`(class = "container",
            h.h1("Emacs Overlay Mapping"),
            h.p(
                "This is a mapping of commits from the ",
                h.a(href = OVERLAY_URL, "nix-community/emacs-overlay"),
                " repository to the then-current HEAD commit from ",
                h.a(href = SAVANNAH_URL, "emacs git (savannah)"),
                ". The aim of this is to provide a way of pinning the emacs overlay flake to a specific commit of emacs HEAD."
            ),
            h.hr(),
            h.h2(
                "This table was generated ",
                $times.now().format("yyyy-MM-dd hh:mm:ss")
            ),
            h.p(
                "A machine readable variant is avaliable ",
                h.a(href = "./overlay-mapping.json", "here"),
                "."
            ),
            h.table(
                h.thead(
                    h.tr(
                        h.th("Commit Time"),
                        h.th("Overlay Commit"),
                        h.th("Emacs Commit"),
                        h.th("Emacs Version")
                    )
                ),
                h.tbody(
                    data.map(v =>
                        h.tr(
                            h.td(v.overlay_ts),
                            h.td(
                                h.`div`(class = "tbl-r",
                                    h.code(v.overlay_rev),
                                    h.a(href = &"{OVERLAY_URL}/commit/{v.overlay_rev}",
                                        h.img(src = "./link.png", alt = "Open emacs-overlay repo", width = "20", height = "20")
                                    )
                                )
                            ),
                            h.td(
                                h.`div`(class = "tbl-r",
                                    h.code(v.emacs_rev),
                                    h.a(href = &"{SAVANNAH_URL}/commit/?id={v.emacs_rev}",
                                        h.img(src = "./link.png", alt = "Open emacs-savannah repo", width = "20", height = "20")
                                    )
                                )
                            ),
                            h.td(v.emacs_version)
                        )
                    ).join("")
                )
            )
        )
    )

    writeFile(to, $h.html(head, body))

proc main(): void =
    var commit = "latest"
    if paramCount() > 0:
      commit = paramStr(1)
    let targetPath = getEnv("DEPLOY_PROD", ".")
    m("Deploying to " & targetPath)
    let dp = os.absolutePath("./repo-emacs-overlay")
    m(&"Repo = {dp}")
    m(&"Target commit = {commit}")
    ensureLocalCopy(OVERLAY_URL, dp)
    ensureAtCommit(commit, dp)
    let hashes = loadHashes(dp)
    var mapping: seq[HashInfo] = @[]
    for idx, hash in enumerate(hashes):
        m(&"checking {idx + 1:>4}/{hashes.len:>4}: {hash.hash}")
        mapping.add(loadEmacsMasterInfo(dp, hash))
    writeFile(&"{targetPath}/overlay-mapping.json", $mapping.toJson)
    toIndex(&"{targetPath}/overlay-mapping.html", mapping)

main()
